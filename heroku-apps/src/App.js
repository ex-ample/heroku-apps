import React, { useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { SubscribeEvents, UnsubscribeEvents, IsSubscribed } from './components/providers/SSE'

import Container from 'react-bootstrap/Container';
import { Button, Col, Form, Row, Stack } from 'react-bootstrap';

import Graph from "react-graph-vis";

const options = {
  layout: {
    hierarchical: true
  },
  edges: {
    color: "#000000"
  },
  height: "500px"
};

const events = {
  select: function (event) {
    var { nodes, edges } = event;
  }
};

const graph = {
  nodes: [
    { id: "client", label: "This is You", title: "This You" },
  ],
  edges: [

  ]
};

function App() {
  const [connected, setConnected] = useState(false)
  const [nodeNameInvalid, setNodeNameInvalid] = useState(false)
  const [nodeName, setNodeName] = useState("");
  const [graphData, setGrapData] = useState(graph);

  const onConnect = () => {
    if (!nodeName || nodeName.replace(" ", "") === "") {
      setNodeNameInvalid(true);
      return;
    }

    if (IsSubscribed) UnsubscribeEvents();

    SubscribeEvents({
      path: "/stream",
      clientID: nodeName,
      csrfToken: "",
      onMessage: (data) => {
        if (data) {
          console.log(data);
          const { nodes, edges } = JSON.parse(data);
          const mapped = nodes.map(node => {
            if (node.id === nodeName) {
              node.title = "Yours";
              node.label = "Yours";
              node.color = "green"
              return node;
            }
            return node;
          });
          setGrapData({ nodes: mapped, edges: edges });
        }
      }
    });

    setNodeNameInvalid(false);
    setConnected(true);
  }

  const onDisconnect = () => {
    setConnected(false);
    UnsubscribeEvents();
    setGrapData(graph);
  };

  return (
    <div className="App">
      <Container fluid="md">
        <Row>
          <Col>
            <Graph
              graph={graphData}
              options={options}
              events={events}
              getNetwork={network => {
                //  if you want access to vis.js network api you can set the state in a parent component using this property
              }}
            />
          </Col>
          <Col lg={3}>
            <Stack gap={3}>
              <Form.Group className='mb3' controlId='formBasicName'>
                <Form.Label>Node Name</Form.Label>
                <Form.Control type='text'
                  placeholder='Your displayed node name'
                  isInvalid={nodeNameInvalid}
                  disabled={connected}
                  value={nodeName}
                  onChange={ev => setNodeName(ev.target.value)}>
                </Form.Control>
                <Form.Text>* this field is required and cannot be empty</Form.Text>
              </Form.Group>
              {
                !connected ?
                  <Button variant='primary'
                    type='button'
                    onClick={onConnect}>Connect</Button> :
                  <Button
                    variant='danger'
                    type='button'
                    onClick={onDisconnect}>Disconnect</Button>
              }
            </Stack>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
