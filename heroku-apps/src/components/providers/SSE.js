import { NativeEventSource, EventSourcePolyfill } from 'event-source-polyfill';

const EventSource = EventSourcePolyfill || NativeEventSource;
// OR: may also need to set as global property
// global.EventSource = EventSourcePolyfill || NativeEventSource;

var SSE;

const SubscribeEvents = ({ path, clientID, csrfToken, onMessage = console.log, onError = console.log }) => {
    if (!SSE) {
        SSE = new EventSource(path, {
            headers: {
                'X-Client-ID': clientID,
                'Token': csrfToken
            }
        });
        SSE.addEventListener('update', (ev) => onMessage(ev.data));
        SSE.onmessage = console.log;
        SSE.onerror = onError;
    }
};

const UnsubscribeEvents = () => {
    if (SSE) {
        SSE = SSE.close();
    }
};

const IsSubscribed = () => {
    return SSE && true;
};

export { SubscribeEvents, UnsubscribeEvents, IsSubscribed };