import http from 'k6/http';
import { check } from 'k6';

const baseURL = __ENV.TARGET_ADDR || "http://127.0.0.1:3000";
import { uuidv4 } from "https://jslib.k6.io/k6-utils/1.4.0/index.js";

export const options = {
  discardResponseBodies: true,

  // scenarios: {
  //   ramp_up: {
  //     executor: 'ramping-vus',
  //     startVUs: 0,
  //     stages: [
  //       { duration: '20s', target: 1 },
  //       { duration: '10s', target: 0 },
  //     ],
  //     gracefulRampDown: '10s',
  //   },
  //   // constant_vus: {
  //   //   startTime: '50s',
  //   //   executor: 'constant-vus',
  //   //   vus: 100,
  //   //   duration: '1m',
  //   // },
  // },
  
  scenarios: {
    ramp_up: {
      executor: 'ramping-vus',
      startVUs: 0,
      stages: [
        { duration: '20s', target: 10 },
        { duration: '10s', target: 0 },
      ],
      gracefulRampDown: '10s',
    },
    // constant_vus: {
    //   startTime: '50s',
    //   executor: 'constant-vus',
    //   vus: 1000,
    //   duration: '1m',
    // },
    // constant_request_rate: {
    //   executor: 'constant-arrival-rate',
    //   startTime: '50s',
    //   rate: 1000,
    //   timeUnit: '1s', // 1000 iterations per second, i.e. 1000 RPS
    //   duration: '30s',
    //   preAllocatedVUs: 100, // how large the initial pool of VUs would be
    //   maxVUs: 200, // if the preAllocatedVUs are not enough, we can initialize more
    // },
  },
};

const endpointsConfig = [
  {
    path: "/pooling",
    method: "GET",
    headers: {
      "X-Client-ID": uuidv4()
    },
    // tests: {
    //   "response code was 200": (res) => res.status == 200 // && res.body.data.package_families.length === 77
    // },
    tags: {
      testName: "health check"
    }
  },
  // {
  //   path: "/factorial",
  //   method: "POST",
  //   bodyReq: { "number": 10},
  //   tests: {
  //     "response code was 200": (res) => res.status == 200 // && res.body.data.package_families.length === 77
  //   },
  //   tags: {
  //     testName: "factorial 10"
  //   }
  // }
  // ,
  // {
  //   path: "/factorial",
  //   method: "POST",
  //   bodyReq: { "number": 100},
  //   tests: {
  //     "response code was 200": (res) => res.status == 200 // && res.body.data.package_families.length === 77
  //   },
  //   tags: {
  //     testName: "factorial 100"
  //   }
  // }
  // ,
  // {
  //   path: "/redis",
  //   method: "POST",
  //   bodyReq: { "op": "SET", "key": "TESTKEY", "ttl": 10, "data": "hello world"},
  //   tests: {
  //     "response code was 200": (res) => res.status == 200 // && res.body.data.package_families.length === 77
  //   },
  //   tags: {
  //     testName: "redis SET op"
  //   }
  // }
  // ,
  // {
  //   path: "/redis",
  //   method: "POST",
  //   bodyReq: { "op": "GET", "key": "TESTKEY" },
  //   tests: {
  //     "response code was 200": (res) => res.status == 200 // && res.body.data.package_families.length === 77
  //   },
  //   tags: {
  //     testName: "redis GET op"
  //   }
  // }
  // ,
];

const execute = ({path, method, bodyReq, tests, tags, headers}) => {
  // const headers = {
  //   'Content-Type':'application/json',
  // };
  
  const resp = http.request(
    method,
    baseURL+path,
    JSON.stringify(bodyReq),
    {headers: headers}
  );
  
  resp.body = JSON.parse(resp.body);

  // check(resp, tests, tags);
};

export default function () {

  endpointsConfig.forEach(execute);
  
}
