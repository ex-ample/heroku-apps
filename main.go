package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func flushMessage(w http.ResponseWriter) {
	if flusher, ok := w.(http.Flusher); ok && flusher != nil {
		flusher.Flush()
	}
}

type ClientConnection struct {
	clientId    string
	ctx         context.Context
	chanMessage chan []byte
}

type Message struct {
	clientId    string
	message     []byte
	isBroadcast bool
}

type SSEBroker struct {
	clientsConn   map[string]*ClientConnection
	chanClientIn  chan *ClientConnection
	chanMessage   chan *Message
	chanClosedReq chan string
	closed        bool
	tick          *time.Ticker
	hostname      string
}

func (sb *SSEBroker) Req(resp http.ResponseWriter, req *http.Request) {
	clientId := req.Header.Get("X-Client-ID")

	if clientId == "" {
		resp.WriteHeader(403)
		resp.Write([]byte("required parameters not found"))
		return
	}

	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	resp.Header().Set("Content-Type", "text/event-stream")
	resp.Header().Set("Cache-Control", "no-cache")
	resp.Header().Set("Connection", "keep-alive")

	chanMessage := make(chan []byte)
	sb.chanClientIn <- &ClientConnection{clientId: clientId, ctx: req.Context(), chanMessage: chanMessage}
	for {
		select {
		case <-req.Context().Done():
			sb.chanClosedReq <- clientId
			return

		case message := <-chanMessage:
			flusher := resp.(http.Flusher)
			_, err := resp.Write(message)
			logWhenError(err)
			flusher.Flush()
		}
	}
}

func (sb *SSEBroker) Update(clientId string, message []byte) {
	sb.chanMessage <- &Message{clientId: clientId, message: message}
}

func (sb *SSEBroker) Broadcast(message []byte) {
	sb.chanMessage <- &Message{message: message, isBroadcast: true}
}

func (sb *SSEBroker) ReportGraph() {
	var connectedClient []string

	for clientId := range sb.clientsConn {
		connectedClient = append(connectedClient, clientId)
	}

	graphData := toGraphData(sb.hostname, connectedClient)
	bufGraphData, graphDataErr := json.Marshal(&graphData)
	logWhenError(graphDataErr)

	sb.Broadcast([]byte(fmt.Sprintf("event: update\ndata: %s\n\n", string(bufGraphData))))
}

func (sb *SSEBroker) Start(ctx context.Context) {
	go func(ctx context.Context, sse *SSEBroker) {

		for {
			select {

			case <-sb.tick.C:
				for _, client := range sb.clientsConn {
					client.chanMessage <- []byte("event: heartbeat\ndata: 1\n\n")
				}

			case <-ctx.Done():
				for clientId, clientReq := range sb.clientsConn {
					clientReq.ctx.Done()
					close(clientReq.chanMessage)
					delete(sb.clientsConn, clientId)
				}
				return

			case clientIn := <-sb.chanClientIn:
				if clientIn.clientId != "" {

					if client, ok := sb.clientsConn[clientIn.clientId]; ok {
						close(client.chanMessage)
						client.chanMessage = nil
						delete(sb.clientsConn, client.clientId)
					}

					sb.clientsConn[clientIn.clientId] = clientIn
					sb.ReportGraph()
				}

			case clientId := <-sb.chanClosedReq:
				if client, ok := sb.clientsConn[clientId]; ok {
					close(client.chanMessage)
					client.chanMessage = nil
					delete(sb.clientsConn, clientId)
				}

				sb.ReportGraph()

			case message := <-sb.chanMessage:
				if message.isBroadcast {
					for _, client := range sb.clientsConn {
						client.chanMessage <- message.message
					}
				} else {
					if client, ok := sb.clientsConn[message.clientId]; ok {
						client.chanMessage <- message.message
					}
				}
			}
		}

	}(ctx, sb)
}

type GraphNode struct {
	Id    string `json:"id"`
	Label string `json:"label"`
	Title string `json:"title"`
	Color string `json:"color,omitempty"`
}

type GraphEdge struct {
	From string `json:"from"`
	To   string `json:"to"`
}

type GraphData struct {
	Nodes []GraphNode `json:"nodes"`
	Edges []GraphEdge `json:"edges"`
}

func toGraphData(hostname string, client []string) GraphData {
	res := GraphData{Nodes: []GraphNode{{Id: hostname, Title: hostname, Label: hostname, Color: "red"}}}
	for _, connectedClient := range client {
		res.Edges = append(res.Edges, GraphEdge{From: connectedClient, To: hostname})
		res.Nodes = append(res.Nodes, GraphNode{Id: connectedClient, Title: connectedClient, Label: connectedClient})
	}

	return res
}

func logWhenError(err error) {
	if err != nil {
		log.Println(err)
	}
}

var port = os.Getenv("PORT")

func main() {

	hostname, hostnameErr := os.Hostname()
	logWhenError(hostnameErr)

	if port == "" {
		port = "3000"
	}

	broker := &SSEBroker{
		clientsConn:   map[string]*ClientConnection{},
		chanClientIn:  make(chan *ClientConnection, 1),
		chanMessage:   make(chan *Message, 1),
		chanClosedReq: make(chan string, 1),
		tick:          time.NewTicker(time.Second * 15),
		hostname:      hostname,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	broker.Start(ctx)

	http.Handle("/", http.FileServer(http.Dir("./public")))
	http.HandleFunc("/stream", broker.Req)

	log.Fatal("HTTP server error: ", http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
